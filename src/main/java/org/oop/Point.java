package org.oop;

public class Point {
    private double x;
    private double y;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double calculateDistance(Point point){
        double xDistance = x - point.x;
        double yDistance = y - point.y;

        return Math.sqrt(Math.pow(xDistance,2) + Math.pow(yDistance, 2));
    }

    public double calculateDirection(Point point){
        double xDistance = x - point.x;
        double yDistance = y - point.y;

        return Math.atan2(yDistance, xDistance);
    }
}