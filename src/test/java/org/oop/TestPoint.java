package org.oop;

import org.junit.Test;
import java.awt.*;
import static org.junit.Assert.assertEquals;

public class TestPoint {
    @Test
    void twoPointsWithSameXAndYCoordinatesShouldHaveADistanceOfZero() {
        Point origin = new Point(0, 0);
        assertEquals(0, origin.calculateDistance(origin));
    }

    @Test
    void distanceBetweenOriginAndPointsOnUnitCircleShouldBeOne() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(1, 0);
        Point point2 = new Point(0, 1);

        assertEquals(1, origin.calculateDistance(point1));
        assertEquals(1, origin.calculateDistance(point2));
    }

    @Test
    void distanceBetweenTwoOppositePointsOnUnitCircleShouldBeTwo() {
        Point point1 = new Point(1, 0);
        Point point2 = new Point(-1, 0);

        assertEquals(2, point1.calculateDistance(point2));
    }

    @Test
    void originAndPointOnPostiveXAxisShouldBeZeroRadiansAway() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(1, 0);
        Point point2 = new Point(3, 0);

        assertEquals(0, origin.calculateDistance(point1));
        assertEquals(0, origin.calculateDistance(point2));
    }

    @Test
    void originAndPointOnNegativeXAxisShouldBePiRadiansAway() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(-1, 0);
        Point point2 = new Point(-3, 0);

        assertEquals(Math.PI, origin.calculateDistance(point1));
        assertEquals(Math.PI, origin.calculateDistance(point2));
    }

    @Test
    void originAndPointOnYAxisShouldBeHalfPiRadiansAway() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(0, 1);
        Point point2 = new Point(0, 3);

        assertEquals(Math.PI / 2, origin.calculateDistance(point2));
        assertEquals(Math.PI / 2, origin.calculateDistance(point1));
    }
}
